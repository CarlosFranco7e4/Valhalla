﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy", order = 3)]
public class Enemy : Character
{
    public float BodyDamage;
    public float BodyKnockback;
    public float WeaponDamage;
    public float WeaponKnockback;
    public float AnimationSpeed;
    public float Weight;
    public float RangedCastTime;
    public AudioClip AudioDeath;
    public AudioClip AudioHit;
}
