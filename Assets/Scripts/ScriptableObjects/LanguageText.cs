﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "LanguageTexts", menuName = "LanguageTexts", order = 0)]
public class LanguageText : ScriptableObject
{
    public string Title;
    public LanguageText DefaultTexts;
    public Dictionary<string, string> TextDict;
    [SerializeField]
    private MyDictionary[] TextArray;

    public void InitValues()
    {
        TextDict = new Dictionary<string, string>();
        foreach (MyDictionary text in TextArray)
        {
            TextDict.Add(text.Key, text.Value);
        }
    }

    public string GetValue(string key)
    {
        if (TextDict.ContainsKey(key))
        {
            return TextDict[key];
        }
        else if(DefaultTexts!=null)
        {
            return DefaultTexts.GetValue(key);
        }
        return "NaN";
    }
}

[Serializable]
public struct MyDictionary
{
    public string Key;
    public string Value;
}
