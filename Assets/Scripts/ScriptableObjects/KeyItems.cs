﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Key items", menuName = "Items/KeyItems", order = 3)]
public class KeyItems : Items
{
    public bool InPossesion;
}
