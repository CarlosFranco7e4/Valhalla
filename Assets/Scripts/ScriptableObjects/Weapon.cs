﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Items/Weapon", order = 1)]
public class Weapon : Items
{
    public float Damage;
    public float AnimationSpeed;    
    public WeaponType Type;
    public bool BasicWeapon;
    
    public enum WeaponType
    {
        Axe,
        Sword,
        Lance,
        Hammer,
        Mace
    }
}
