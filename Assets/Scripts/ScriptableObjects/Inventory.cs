﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 1)]
public class Inventory : ScriptableObject
{
    public List<Weapon> Weapons;
    public List<Armor> Armor;
    public List<Runes> Runes;
    public List<KeyItems> KeyItems;
    public List<StatOrbs> StatOrbs;
}
