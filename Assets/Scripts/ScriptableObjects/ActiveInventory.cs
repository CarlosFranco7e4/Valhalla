﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActiveInventory", menuName = "ScriptableObjects/ActiveInventory", order = 1)]
public class ActiveInventory : ScriptableObject
{
    public Armor ArmorSet;
    public Armor ArmorShield;
    public Weapon[] Weapons = new Weapon[3];
    public RuneList[] Runes = new RuneList[5];
}
[Serializable]
public class RuneList
{
    public Runes[] Runes = new Runes[3];
}
