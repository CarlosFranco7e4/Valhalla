﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Stat Orb", menuName = "Items/Stat Orbs", order = 2)]
public class StatOrbs : Items
{
    public int StatsUp;
}
