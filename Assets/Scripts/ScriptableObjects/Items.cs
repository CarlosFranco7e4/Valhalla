﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : ScriptableObject
{
    public string Name;
    public Sprite Icon;
    public GameObject Item;
    public string Description;
}
