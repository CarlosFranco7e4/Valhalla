﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Runes", menuName = "Items/Runes", order = 1)]
public class Runes : Items
{    
    public RuneType Type;
    public float StatUp;

    public enum RuneType
    {
        Triangle,
        Square,
        Circle
    }
}