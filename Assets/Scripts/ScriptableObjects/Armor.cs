﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor piece", menuName = "Items/Armor", order = 3)]
public class Armor : Items
{
    public int DefenseBoost;
    public ArmorType Type;
    
    public enum ArmorType
    {
        Shield,
        Armor
    }
}
