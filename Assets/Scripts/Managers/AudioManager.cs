﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] Sounds;

    public static AudioManager instance;
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }            
        else
        {
            Destroy(gameObject);
            return;
        }            

        DontDestroyOnLoad(gameObject);

        foreach (Sound so in Sounds)
        {
            so.Source = gameObject.AddComponent<AudioSource>();
            so.Source.clip = so.Clip;

            so.Source.volume = so.Volume;
            so.Source.loop = so.Loop;
            so.Source.outputAudioMixerGroup = so.audioMixerGroup;
        }
    }

    private void Start()
    {
        Play("Menu Theme");
    }

    public void Play(string name)
    {
        Sound so = Array.Find(Sounds, sound => sound.Name == name);
        if (so == null)
            return;
        so.Source.Play();
    }

    public void Pause(string name)
    {
        Sound so = Array.Find(Sounds, sound => sound.Name == name);
        if (so == null)
            return;
        so.Source.Pause();
    }

    public void PauseAll()
    {
        foreach (Sound so in Sounds)
        {
            if (so == null)
                return;
            so.Source.Pause();
        }                
    }

    public void UnPause(string name)
    {
        Sound so = Array.Find(Sounds, sound => sound.Name == name);
        if (so == null)
            return;
        so.Source.UnPause();
    }

    public void UnPauseAll()
    {
        foreach (Sound so in Sounds)
        {
            if (so == null)
                return;
            so.Source.UnPause();
        }
    }

    public void Stop(string name)
    {
        Sound so = Array.Find(Sounds, sound => sound.Name == name);
        if (so == null)
            return;
        so.Source.Stop();
    }

    public void StopAll()
    {
        foreach (Sound so in Sounds)
        {
            if (so == null)
                return;
            so.Source.Stop();
        }
    }
}
