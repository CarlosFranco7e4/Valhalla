﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public Weapon EmptyWeapon;
    public Armor EmptyArmor;
    public Runes[] EmptyRunes;
    internal List<long> items=new List<long>();
    public LevelUIController LevelUIController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitLevelUIController(LevelUIController newPUIC)
    {
        LevelUIController = newPUIC;
    }

    public void ExitLevel()
    {
        Save();
        Time.timeScale = 1;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        Destroy(PlayerManager.Instance.gameObject);
        Destroy(this.gameObject);
    }
    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex,LoadSceneMode.Single);
        PlayerManager.Instance.RevivePlayer();
    }
    public void Save()
    {
        Debug.Log("SaveData");
    }
}
