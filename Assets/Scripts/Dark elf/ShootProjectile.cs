using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootProjectile : MonoBehaviour
{

    public GameObject Projectile;
    public float ProkectileSpeed;
    public GameObject ShootPoint;
    public GameObject target;
    private Vector3 lookTarget;
    
    private void Update() {
      if(target==null){

          target = GameObject.Find("PlayerTarget");
      }
      lookTarget = target.transform.position - transform.position;
      

    }
    public void shoot(){
      gameObject.GetComponentInParent<Animator>().SetTrigger("Shoot");
      GameObject clone = Instantiate(Projectile, ShootPoint.transform.position, transform.rotation);
      clone.GetComponent<ProjectileMovement>().ProjectileSpeed = ProkectileSpeed;
      clone.GetComponent<ProjectileMovement>().damage = gameObject.GetComponent<EnemyHitReceiveAndDeath>().Stats.WeaponDamage;

    }
}
