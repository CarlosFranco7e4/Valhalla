﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeCamera : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public GameObject camTransform;

    // How long the object should shake for.
    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    Vector3 originalPos;

    void Start()
    {
        if (camTransform == null)
        {
            camTransform = GameObject.Find("Main Camera");
            originalPos = camTransform.transform.position;
        }
    }

    void Update()
    {
        
        if (shakeDuration > 0)
        {
            originalPos = camTransform.transform.position;
            camTransform.transform.position  = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
            camTransform.transform.position = new Vector3(camTransform.transform.position.x,camTransform.transform.position.y, originalPos.z);
        }
        else
        {
            shakeDuration = 0f;
            //camTransform.transform.position = originalPos;
        }
    }

    public void shakeCamera()
    {
        shakeDuration = 0.7f;
        AudioManager.instance.Play("BossJumpAttack");
    }
}