﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    private bool hit = true;
    public SpriteRenderer[] NeededRuneImages;
    public Runes NeededRune;
    public int RuneQuantity;
    public Items ItemDropInfo;
    public GameObject ItemDropPrefab;
    public ItemType ItemDropType;
    private Animator chestAnimator;
    public int Id;

    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.Instance.items.Contains(Id) && Id != 0)
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
        chestAnimator = gameObject.GetComponent<Animator>();
        showNeededRunes();
    }
    
    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<AttackController>().attacking == true && other.CompareTag("Weapon") && hit == true && isCorrectWeaponRunes())
        {   
            hit = false;
            chestAnimator.SetTrigger("Open");
            AudioManager.instance.Play("ChestOpening");
            hideNeededRunes();
        }
    }

    public void CreateItem()
    {
        GameObject newItem=null;
        Vector3 newPosition = gameObject.transform.parent.gameObject.transform.position + new Vector3(0, 1.5f, 0);
        switch (ItemDropType)
        {
            case ItemType.Orb:
                Debug.Log("Hit Chest 2");
                newItem =  Instantiate(ItemDropPrefab, newPosition, Quaternion.identity);
                break;
            case ItemType.Weapon:
            case ItemType.Armor:
            case ItemType.Rune:
                newItem=Instantiate(ItemDropPrefab, newPosition, Quaternion.identity);
                newItem.GetComponentInChildren<ItemController>().Item = ItemDropInfo;
                newItem.GetComponentInChildren<ItemController>().SetIcon();
                break;
        }
        if (newItem != null)
        {
            newItem.GetComponentInChildren<ItemController>().Id = Id;
        }
    }

    private void showNeededRunes()
    {
        for (int i = 0; i < RuneQuantity; i++)
        {
            NeededRuneImages[i].gameObject.SetActive(true);
            NeededRuneImages[i].sprite = NeededRune.Icon;
        }
    }

    private void hideNeededRunes()
    {
        foreach(SpriteRenderer neededRune in NeededRuneImages)
        {
            neededRune.gameObject.SetActive(false);
        }
    }

    private bool isCorrectWeaponRunes()
    {
        if (PlayerManager.Instance.ActiveWeaponIndex != 0)
        {
            return false;
        }
        for (int i = 0; i < RuneQuantity; i++)
        {
            if (!PlayerManager.Instance.ActiveInventory.Runes[0].Runes[i].Equals(NeededRune))
            {
                return false;
            }
        }
        return true;
    }

    public void Dissapear() {
        Destroy(gameObject.transform.parent.gameObject);
    }
}
