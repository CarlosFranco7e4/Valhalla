﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    public Animator anim;
    public GameObject weapon;
    public bool attacking = false;
    public bool hasAttack2;
    public bool hasAttack3;
    public bool sameAttack;

    void Update()
    {
        anim.SetBool("HasAttack2", hasAttack2);
        anim.SetBool("HasAttack3", hasAttack3);
        
       if (Input.GetButtonDown("Fire1") && PlayerManager.Instance.MovementAvailable == true){
            
            anim.SetTrigger("Attack");            
        }
       
    }

    void AtackFinished(){        
        anim.SetBool("AtacFinished", true);
    }
    void AtackFinishedFalse(){        
        anim.SetBool("AtacFinished", false);
    }

    public void ResetEnemyHitReceiver()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length>0){
            foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")){
                enemy.GetComponent<EnemyHitReceiveAndDeath>().ResetHit();
            }
        }
    }
}
