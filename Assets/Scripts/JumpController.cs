﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    /*private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 2.0f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;

    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }*/
    public float JumpForce;
    public float PlatformDiastance;
    public int MaxJumps = 1;
    private int jumps = 1;
    private Rigidbody rigidbody;
    private Animator ani;
    private bool jumFire;
    // Start is called before the first frame update
    void Start()
    {
        jumFire = false;
        rigidbody = gameObject.GetComponent<Rigidbody>();
        ani = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Physics.Raycast(transform.position, -Vector3.up, PlatformDiastance))
        {
            ani.SetBool("onair", false);
            jumps = 1;
        }
        else
        {
            Debug.Log("Estic caient");
            ani.SetBool("onair", true);
        }

        Debug.DrawRay(transform.position, -Vector3.up*PlatformDiastance,Color.red,1);

        if (Input.GetButtonDown("Jump")&&Physics.Raycast(transform.position, -Vector3.up, PlatformDiastance))
        {
            Debug.Log("FloorJump");
            jumps = 0;
            ani.SetBool("jump", true);
        }
        else if (jumps<MaxJumps&&Input.GetButtonDown("Jump"))
        {
            Debug.Log("FlyJump");
            ani.SetBool("jump", true);
        }
    }

    private void FixedUpdate()
    {
        if (jumFire)
        {
            jump();
        }
    }
    public void jump()
    {
        jumps++;
        rigidbody.velocity = new Vector3(rigidbody.velocity.x,0,rigidbody.velocity.z);
        rigidbody.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
        jumFireOff();
        ani.SetBool("jump", false);

    }

    public void jumFireOn()
    {
        jumFire = true;
    }

    public void jumFireOff()
    {
        jumFire = false;
    }
}
