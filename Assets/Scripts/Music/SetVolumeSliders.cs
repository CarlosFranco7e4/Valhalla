﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;

public class SetVolumeSliders : MonoBehaviour
{    
    public Slider MusicVolume;
    public Slider SEVolume;

    public AudioMixer mixer;

    void Start()
    {
        MusicVolume.value = PlayerPrefs.GetFloat("Music");
        SEVolume.value = PlayerPrefs.GetFloat("SoundEffects");       
    }
}
