﻿using UnityEngine.Audio;
using UnityEngine;
using System;

[Serializable]
public class Sound
{
    public string Name;

    public AudioClip Clip;

    public bool Loop;

    public AudioMixerGroup audioMixerGroup;

    [Range(0,1)]
    public float Volume;

    [HideInInspector]
    public AudioSource Source;
}
