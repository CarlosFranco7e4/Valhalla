﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class SetMixerAudio : MonoBehaviour
{
    public AudioMixer mixer;

    void Start()
    {        
        mixer.SetFloat("Music", Mathf.Log10(PlayerPrefs.GetFloat("Music")) * 20);
        mixer.SetFloat("SoundEffects", Mathf.Log10(PlayerPrefs.GetFloat("SoundEffects")) * 20);
    }
}
