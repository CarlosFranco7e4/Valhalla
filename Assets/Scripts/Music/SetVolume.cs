﻿using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetVolume : MonoBehaviour
{
    public AudioMixer mixer;

    public string mixerName;

    public void SetLevel(float sliderValue)
    {
        mixer.SetFloat(mixerName, Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat(mixerName, sliderValue);
        PlayerPrefs.Save();        
    }
}
