﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    public GameObject Player;
    public float XDif;
    public float YDif;
    public float XFree;
    public float YFree;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float XPlayer = Player.transform.position.x;
        float YPlayer = Player.transform.position.y;
        float XCamera = gameObject.transform.position.x;
        float YCamera = gameObject.transform.position.y;
        float ZCamera = gameObject.transform.position.z;
        if(XPlayer > XCamera + XFree - XDif)
        {
            XCamera = XPlayer - XFree + XDif;
        }else if(XPlayer < XCamera - XFree - XDif)
        {
            XCamera = XPlayer + XFree + XDif;
        }
        if (YPlayer > YCamera + YFree - YDif)
        {
            YCamera = YPlayer - YFree + YDif;
        }
        else if (YPlayer < YCamera - YFree - YDif)
        {
            YCamera = YPlayer + YFree + YDif;
        }
        gameObject.transform.position = new Vector3(XCamera, YCamera, ZCamera);
    }
}
