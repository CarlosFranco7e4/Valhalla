﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SettingsUIController : MonoBehaviour
{
    public Text SoundEffectsText;
    public Text MusicText;    
    public Text MadeByText;
    public Dropdown LangSelector; 
    public GameObject ControlsPanel;
    public GameObject CreditsPanel;

    // Start is called before the first frame update
    void Start()
    {
        reloadTexts();
        generateDropdownLangs();
    }

    public void ExitSettings()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);        
    }

    public void ShowHideControls()
    {
        ControlsPanel.SetActive(!ControlsPanel.activeSelf);
        CreditsPanel.SetActive(false);
    }
    public void ShowHideCredits()
    {
        CreditsPanel.SetActive(!CreditsPanel.activeSelf);
        ControlsPanel.SetActive(false);
    }

    public void ChangeLang()
    {
        GameManager.Instance.SelectedLang = LangSelector.value;
        reloadTexts();
    }

    private void reloadTexts()
    {
        SoundEffectsText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("SoundEffects");
        MusicText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Music");        
        MadeByText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("MadeBy");
    }

    private void generateDropdownLangs()
    {
        LangSelector.options.Clear();
        foreach (LanguageText idiom in GameManager.Instance.Texts)
        {
            LangSelector.options.Add(new Dropdown.OptionData(idiom.Title));
        }
        LangSelector.value = GameManager.Instance.SelectedLang;
        if (GameManager.Instance.SelectedLang == 0)
        {
            LangSelector.value = 1;
            LangSelector.value = 0;
        }
    }
}
