﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUIController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play()
    {
        GameManager.Instance.StartLevel();
        AudioManager.instance.Stop("Menu Theme");
        AudioManager.instance.Play("Level 1 Theme");
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void Settings()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
}
