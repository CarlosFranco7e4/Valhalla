﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUIController : MonoBehaviour
{
    public enum OptionType
    {
        Weapon,
        Armor,
        Rune
    }

    internal int ItemIndex = -1;
    internal int ListIndex = -1;
    internal Items Item;
    public GameObject OptionsPanel;
    public GameObject[] Options;
    public GameObject ActiveIcon;
    public OptionType Type;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        HideIfClickedOutside();
    }

    private void HideIfClickedOutside()
    {
        if (Input.GetMouseButton(0) && OptionsPanel.activeSelf && !RectTransformUtility.RectangleContainsScreenPoint(OptionsPanel.GetComponent<RectTransform>(), Input.mousePosition))
        {
            OptionsPanel.SetActive(false);            
        }
    }

    public void ShowIsActive()
    {
        ActiveIcon.SetActive(true);
        if (Type == OptionType.Rune)
        {
            ActiveIcon.GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneEquipedIcons[ListIndex];
        }
    }

    public void showOptions()
    {
        OptionsPanel.SetActive(true);
        switch (Type)
        {
            case OptionType.Weapon:
                showWeaponOptions();
                break;
            case OptionType.Armor:
                showArmorOptions();
                break;
            case OptionType.Rune:
                showRuneOptions();
                break;
        }
    }

    private void showWeaponOptions()
    {
        if (((Weapon)Item).BasicWeapon)
        {
            if (ActiveIcon.activeSelf)
            {
                Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[1];
            }
            else
            {
                Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[0];
            }
            Options[1].SetActive(false);
            Options[2].SetActive(false);
        }
        else
        {
            if (ActiveIcon.activeSelf)
            {
                if (PlayerManager.Instance.ActiveInventory.Weapons[1].Equals((Weapon)Item))
                {
                    Options[1].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[3];
                    Options[2].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[4];
                }
                else
                {
                    Options[1].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[2];
                    Options[2].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[5];
                }
            }
            else
            {
                Options[1].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[2];
                Options[2].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[4];
            }
            Options[0].SetActive(false);
        }
    }

    private void showArmorOptions()
    {
        switch (((Armor)Item).Type)
        {
            case Armor.ArmorType.Armor:
                if (ActiveIcon.activeSelf)
                {
                    Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[7];
                }
                else
                {
                    Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[6];
                }
                break;
            case Armor.ArmorType.Shield:
                if (ActiveIcon.activeSelf)
                {
                    Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[9];
                }
                else
                {
                    Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.OptionsIcons[8];
                }
                break;
        }
    }

    private void showRuneOptions()
    {
        int activeRuneWeaponIndex = PlayerManager.Instance.PauseUIController.ActiveRuneWeaponIndex;
        switch (((Runes)Item).Type)
        {
            case Runes.RuneType.Triangle:
                Options[1].SetActive(false);
                Options[2].SetActive(false);
                if (activeRuneWeaponIndex == 0)
                {
                    if (ActiveIcon.activeSelf)
                    {
                        Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[1];
                    }
                    else
                    {
                        Options[0].SetActive(true);
                        Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[0];
                    }
                }
                else
                {
                    if (ActiveIcon.activeSelf)
                    {
                        Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[1];
                    }
                    else
                    {
                        Options[0].SetActive(false);
                    }
                }
                break;

            case Runes.RuneType.Square:
                int tmp = activeRuneWeaponIndex + 2;
                Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[tmp * 2];
                Options[1].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[tmp * 2];
                Options[2].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[tmp * 2];
                if (ActiveIcon.activeSelf && ((activeRuneWeaponIndex == 0 && ListIndex == 1) || tmp == ListIndex))
                {
                    Options[ItemIndex].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[(tmp * 2) + 1];
                }
                break;

            case Runes.RuneType.Circle:
                if (activeRuneWeaponIndex == 0)
                {
                    Options[0].SetActive(true);
                    Options[1].SetActive(true);
                    Options[2].SetActive(true);
                    Options[0].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[2];
                    Options[1].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[2];
                    Options[2].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[2];
                }
                else
                {
                    Options[0].SetActive(false);
                    Options[1].SetActive(false);
                    Options[2].SetActive(false);
                }
                if (ActiveIcon.activeSelf)
                {
                    Options[ItemIndex].SetActive(true);
                    Options[ItemIndex].GetComponent<Image>().sprite = PlayerManager.Instance.PauseUIController.RuneOptionsIcons[3];
                }
                break;
        }
    }

    public void equipRune(int position)
    {
        AudioManager.instance.Play("Confirm / Open Inventory");
        int activeRuneWeaponIndex = PlayerManager.Instance.PauseUIController.ActiveRuneWeaponIndex;
        switch (((Runes)Item).Type)
        {
            case Runes.RuneType.Triangle:
                if (ActiveIcon.activeSelf)
                {
                    int unequipPosition = 0;
                    if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[2] != LevelManager.Instance.EmptyRunes[0])
                    {
                        Debug.Log("pos2");
                        unequipPosition = 2;
                    }
                    else if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[1] != LevelManager.Instance.EmptyRunes[0])
                    {
                        unequipPosition = 1;
                        Debug.Log("pos1");
                    }
                    PlayerManager.Instance.ActiveInventory.Runes[0].Runes[unequipPosition] = LevelManager.Instance.EmptyRunes[0];
                    //Uneqip last
                }
                else
                {
                    int equipPosition=0;
                    //Comprovar si es igual
                    if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[0] == (Runes)Item || PlayerManager.Instance.ActiveInventory.Runes[0].Runes[0] == LevelManager.Instance.EmptyRunes[0])
                    {
                        if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[0] == LevelManager.Instance.EmptyRunes[0])
                        {
                            equipPosition = 0;
                        }
                        else
                        if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[1] == LevelManager.Instance.EmptyRunes[0])
                        {
                            equipPosition = 1;
                        }
                        else if (PlayerManager.Instance.ActiveInventory.Runes[0].Runes[2] == LevelManager.Instance.EmptyRunes[0])
                        {
                            equipPosition = 2;
                        }
                        else
                        {
                            equipPosition = -1;
                        }
                    }
                    else
                    {
                        PlayerManager.Instance.ActiveInventory.Runes[0].Runes[0] = LevelManager.Instance.EmptyRunes[0];
                        PlayerManager.Instance.ActiveInventory.Runes[0].Runes[1] = LevelManager.Instance.EmptyRunes[0];
                        PlayerManager.Instance.ActiveInventory.Runes[0].Runes[2] = LevelManager.Instance.EmptyRunes[0];
                    }
                    if (equipPosition != -1)
                    {
                        PlayerManager.Instance.ActiveInventory.Runes[0].Runes[equipPosition] = (Runes)Item;
                    }
                    //True Equip Next
                    //False Unequip all & Equip first
                }
                break;

            case Runes.RuneType.Square:
                int positionToEquip= activeRuneWeaponIndex+2;
                if (activeRuneWeaponIndex==0)
                {
                    positionToEquip = 1;
                }
                if (ActiveIcon.activeSelf)
                {
                    Debug.Log(ListIndex+" "+ ItemIndex);
                    bool toEquip =!(PlayerManager.Instance.ActiveInventory.Runes[positionToEquip].Runes[position] == (Runes)Item);
                    //Uneqip equpiped position
                    PlayerManager.Instance.ActiveInventory.Runes[ListIndex].Runes[ItemIndex] = LevelManager.Instance.EmptyRunes[1];
                    if (/*listOfActiveWeapon || positionOfList*/toEquip)
                    {
                        //Equip position
                        PlayerManager.Instance.ActiveInventory.Runes[positionToEquip].Runes[position] = (Runes)Item;
                    }
                }
                else
                {
                    //Equip position
                    PlayerManager.Instance.ActiveInventory.Runes[positionToEquip].Runes[position] = (Runes)Item;
                }
                break;

            case Runes.RuneType.Circle:
                /*if (activeRuneWeaponIndex == 0)
                {*/
                if (ActiveIcon.activeSelf)
                {
                    //Uneqip equpiped position
                    PlayerManager.Instance.ActiveInventory.Runes[2].Runes[ItemIndex] = LevelManager.Instance.EmptyRunes[2];
                    if (position != ItemIndex)
                    {
                        //Equip position
                        PlayerManager.Instance.ActiveInventory.Runes[2].Runes[position] = (Runes)Item;
                    }
                }
                else
                {
                    //Equip position
                    PlayerManager.Instance.ActiveInventory.Runes[2].Runes[position] = (Runes)Item;
                }
                /*}
                else
                {
                    //Uneqip position
                    PlayerManager.Instance.ActiveInventory.Runes[2].Runes[ItemIndex] = LevelManager.Instance.EmptyRune;
                }*/
                break;
        }
        PlayerManager.Instance.PauseUIController.FillRunesList();
        PlayerManager.Instance.PauseUIController.ReloadActiveRunesIcons();
        PlayerManager.Instance.ReloadTriangleRuneEffects();
        PlayerManager.Instance.ReloadCircleRuneEffects();
    }

    public void equipWeapon(int position)
    {
        AudioManager.instance.Play("Confirm / Open Inventory");
        if (PlayerManager.Instance.ActiveInventory.Weapons[position].Equals((Weapon)Item))
        {
            PlayerManager.Instance.ActiveInventory.Weapons[position] = LevelManager.Instance.EmptyWeapon;
        }
        else
        {
            if (ActiveIcon.activeSelf && position != 0)
            {
                PlayerManager.Instance.ActiveInventory.Weapons[(position * -1) + 3] = LevelManager.Instance.EmptyWeapon;
            }
            PlayerManager.Instance.ActiveInventory.Weapons[position] = (Weapon)Item;
        }
        PlayerManager.Instance.PauseUIController.FillWeaponsList();
        PlayerManager.Instance.PauseUIController.ReloadActiveWeaponsIcons();
        PlayerManager.Instance.WeaponController.PutWeapon(PlayerManager.Instance.ActiveInventory.Weapons[PlayerManager.Instance.ActiveWeaponIndex]);
    }

    public void equipArmor()
    {
        AudioManager.instance.Play("Confirm / Open Inventory");
        Armor armor = (Armor)Item;
        switch (armor.Type)
        {
            case Armor.ArmorType.Armor:
                if (PlayerManager.Instance.ActiveInventory.ArmorSet.Equals(armor))
                {
                    PlayerManager.Instance.ActiveInventory.ArmorSet = LevelManager.Instance.EmptyArmor;
                }
                else
                {
                    PlayerManager.Instance.ActiveInventory.ArmorSet = armor;
                }
                break;
            case Armor.ArmorType.Shield:
                if (PlayerManager.Instance.ActiveInventory.ArmorShield.Equals(armor))
                {
                    PlayerManager.Instance.ActiveInventory.ArmorShield = LevelManager.Instance.EmptyArmor;
                }
                else
                {
                    PlayerManager.Instance.ActiveInventory.ArmorShield = armor;
                }
                PlayerManager.Instance.BlockController.PutShield(PlayerManager.Instance.ActiveInventory.ArmorShield);
                break;
        }
        PlayerManager.Instance.PauseUIController.FillArmorList();
        PlayerManager.Instance.PauseUIController.ReloadActiveArmorIcons();
    }

    public void cancelOptions()
    {
        AudioManager.instance.Play("Cancel / Return");
        OptionsPanel.SetActive(false);
    }
}
