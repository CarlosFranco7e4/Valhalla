﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapZooming : MonoBehaviour
{
    public GameObject canvas;
    [SerializeField]
    private Camera cam;
    private float targetZoom;
    public float zoomFactor = 3f;
    [SerializeField] private float zoomLerpSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        targetZoom = cam.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        float scrollData;
        scrollData = Input.GetAxis("Mouse ScrollWheel");
        Debug.Log(scrollData);
        if (canvas.activeSelf)
        {
            targetZoom = targetZoom - scrollData * zoomFactor;
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
        }
        
    }
}
