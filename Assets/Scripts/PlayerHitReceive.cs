﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitReceive : MonoBehaviour
{
    public bool Invincible;
    public float InvincibleTime;
    public void HitReceive(float damage) {
        if (Invincible == false){
            Invincible = true;
            StartCoroutine(InvincibleCorrutine(InvincibleTime));
            PlayerManager.Instance.PlayerReciveDamage(damage);
            Debug.Log("PlayerHit");
        }
    }
    IEnumerator InvincibleCorrutine(float time) {
        yield return new WaitForSeconds(time);
        Invincible = false;
        Debug.Log("Invincibility finished");

        
    }

}
