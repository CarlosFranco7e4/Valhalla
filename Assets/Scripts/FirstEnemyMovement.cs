﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstEnemyMovement : enemy_movement
{
    public float Speed;

    public float patrol_distance;

    private float actual_pos = 0;

    private float contador = 0;

    private GameObject playerRef;

    private Vector3 inicialPos;
    private FieldOfView fov;
    private Vector3 targetAngles;
    private bool canRotate = true;

    public bool MoveWhileAttack;

    public float RangeOfStartAttack;

    // Start is called before the first frame update
    void Start()
    {
        playerRef = GameObject.FindGameObjectWithTag("Player");
        inicialPos = gameObject.transform.position;
    }

    public void patrol()
    {
        if (actual_pos < patrol_distance)
        {
            transform.Translate(Vector3.forward * Speed * Time.deltaTime);
            actual_pos += (Vector3.forward * Speed * Time.deltaTime).z;

        }
        if (actual_pos >= patrol_distance)
        {
            rotate(5);
        }

    }

    public void followPlayer()
    {
        if(MoveWhileAttack == true){
        transform.position = Vector3.MoveTowards(transform.position, playerRef.transform.position, Speed * 2 * Time.deltaTime);
        }
    }

    public void attackPlayerMelee()
    {
        gameObject.GetComponent<Animator>().SetTrigger("attack");
    }

    public void rotate(int speed)
    {
        gameObject.transform.Rotate(new Vector3(0, speed, 0));
        contador = contador + speed;
        if (contador >= 180)
        {
            contador = 0;
            actual_pos = 0;
        }
    }

    public void rotateFast()
    {
        transform.RotateAround(transform.position, transform.up, 180f);
    }

    protected override void MyUpdate()
    {
        if (Vector3.Distance(playerRef.transform.position, transform.position) <= RangeOfStartAttack) // Ataca al jugador si es troba massa aprop
        {
            // Implementar el gir del enemic si el player es troba darrere

            attackPlayerMelee();
            gameObject.GetComponent<Animator>().SetBool("stop_attack",false);

        }
        else if (gameObject.GetComponent<FieldOfView>().canSeePlayer && Vector3.Distance(playerRef.transform.position, transform.position) >= RangeOfStartAttack)
        {
            gameObject.GetComponent<Animator>().SetTrigger("stop_attack");
            followPlayer();
        }
        else if (!gameObject.GetComponent<FieldOfView>().canSeePlayer) //No veu al jugador
        {
            gameObject.GetComponent<Animator>().SetTrigger("stop_attack");
            patrol();
        }
    }
}
