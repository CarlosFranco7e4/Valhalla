﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsController : MonoBehaviour
{
    public List<EffectLevels> Effects;

    public void ActivateEffect(int index, int level)
    {
        Effects[index].EffectLevel[level - 1].SetActive(true);   
        if (index == 0 || index == 1)
        {
            AudioManager.instance.Play("Fire/Poison on weapon");
            AudioManager.instance.Pause("Fire/Poison on weapon");
        }
        else if (index == 2)
        {
            AudioManager.instance.Play("IceAura");
            AudioManager.instance.Pause("IceAura");
        }
        else if (index == 3)
        {
            AudioManager.instance.Play("ThunderAura");
            AudioManager.instance.Pause("ThunderAura");
        }
    }

    public void ReactivateEffect(int index, int level)
    {
        Effects[index].EffectLevel[level - 1].SetActive(true);   
        if (index == 0 || index == 1)
        {
            AudioManager.instance.Play("Fire/Poison on weapon");           
        }
        else if (index == 2)
        {
            AudioManager.instance.Play("IceAura");            
        }
        else if (index == 3)
        {
            AudioManager.instance.Play("ThunderAura");            
        }
    }

    public void DesactiveEffects()
    {
        foreach(EffectLevels effLev in Effects)
        {            
            foreach (GameObject eff in effLev.EffectLevel)
            {
                eff.SetActive(false);
                AudioManager.instance.Stop("Fire/Poison on weapon");
                AudioManager.instance.Stop("IceAura");
                AudioManager.instance.Stop("ThunderAura");
            }
        }
    }
}

[Serializable]
public class EffectLevels
{
    public List<GameObject> EffectLevel;
}
