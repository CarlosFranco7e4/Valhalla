﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public Weapon WeaponInfo;
    public float Damage;
    public GameObject ActiveWeapon;
    private int activeWeaponRuneList = 1;
    void Start()
    {
        PlayerManager.Instance.InitWeaponController(this);
        PutWeapon(PlayerManager.Instance.GetActiveWeapon());
    }

    void Update()
    {
        if (PlayerManager.Instance.MovementAvailable)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                PutWeapon(PlayerManager.Instance.ChangeWeapon(true));
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                PutWeapon(PlayerManager.Instance.ChangeWeapon(false));
            }
        }
    }

    public void PutWeapon(Weapon newWeapon)
    {
        WeaponInfo = newWeapon;
        Damage = WeaponInfo.Damage;
        Destroy(ActiveWeapon);
        ActiveWeapon = Instantiate(WeaponInfo.Item,gameObject.transform);
        ActiveWeapon.transform.parent = gameObject.transform;
        LevelManager.Instance.LevelUIController.ReloadWeapons();
        activeWeaponRuneList = PlayerManager.Instance.ActiveWeaponIndex + 2;
        if (activeWeaponRuneList == 2)
        {
            activeWeaponRuneList = 1;
        }
        ReloadVisualEffects();
    }

    public float GetDamage()
    {
        float toAdd = 0;
        foreach (Runes rune in PlayerManager.Instance.ActiveInventory.Runes[activeWeaponRuneList].Runes)
        {
            if (rune.Name.Equals("MoreDamage"))
            {
                toAdd += rune.StatUp;
            }
        }
        return Damage + toAdd;
    }

    public void ReloadVisualEffects()
    {
        if (PlayerManager.Instance.ActiveWeaponIndex == 0 && WeaponInfo != LevelManager.Instance.EmptyWeapon)
        {
            EffectsController weaponEffects = ActiveWeapon.GetComponentInChildren<EffectsController>();
            weaponEffects.DesactiveEffects();            
            int index = -1;
            switch (getRune(0).Name)
            {
                case "Fire":
                    index = 0;                    
                    break;
                case "Potion":                    
                    index = 1;
                    break;
                case "Ice":                    
                    index = 2;
                    break;
                case "Thunder":                    
                    index = 3;
                    break;
            }
            if (index!= - 1){
                int level = countRunes();
                weaponEffects.ReactivateEffect(index, level);                
            }
        }
    }
    private Runes getRune(int position)
    {
        return PlayerManager.Instance.ActiveInventory.Runes[0].Runes[position];
    }

    private int countRunes()
    {
        int level = 1;
        if (getRune(0).Equals(getRune(2)))
        {
            level = 3;
        }
        else if (getRune(0).Equals(getRune(1)))
        {
            level = 2;
        }
        return level;
    }
}
