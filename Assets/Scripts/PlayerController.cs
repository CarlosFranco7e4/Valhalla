﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float MaxHp;
    public float Hp;
    public float DamageMult;
    public float HpFlaskRestore;
    public Character PlayerStats;
    private void Start() {
        
    }
    void Update()
    {
       if (Hp <= 0){
           Death();
       } 
    }

    public void LoseHp(float damage){
        Hp -= damage;
    }

    public void Death(){
        Destroy(gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
