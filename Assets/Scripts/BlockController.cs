﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    public Armor ArmorInfo;
    public GameObject ActiveShield;
    public Animator ani;

    private bool canBlock = true;

    // Start is called before the first frame update
    void Start()
    {
        PlayerManager.Instance.InitBlockController(this);
        PutShield(PlayerManager.Instance.GetActiveArmor());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Block") && canBlock)
        {            
            StartCoroutine(Block());           
        }
    }

    public void PutShield(Armor newShield)
    {
        ArmorInfo = newShield;
        if (ArmorInfo.name == "EmptyArmor")
        {
            canBlock = false;
        }
        else
        {
            canBlock = true;
        }
        Destroy(ActiveShield);
        ActiveShield = Instantiate(ArmorInfo.Item, gameObject.transform);
        ActiveShield.transform.parent = gameObject.transform;
    }

    IEnumerator Block()
    {
        canBlock = false;
        ani.SetTrigger("Block");
        ani.gameObject.tag = "Untagged";
        yield return new WaitForSeconds(0.5F);
        ani.gameObject.tag = "Player";
        yield return new WaitForSeconds(2);
        canBlock = true;
    }
}
