﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum NexStageOptions
{
    Next,
    Previous
}
public class NextStage : MonoBehaviour
{
    public NexStageOptions SceneTo;
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            switch (SceneTo)
            {
                case NexStageOptions.Next:
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1,LoadSceneMode.Single);
                    AudioManager.instance.Play("Level 2 Theme");
                    AudioManager.instance.Stop("Level 1 Theme");
                    break;
                case NexStageOptions.Previous:
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1, LoadSceneMode.Single);
                    AudioManager.instance.Play("Level 1 Theme");
                    AudioManager.instance.Stop("Level 2 Theme");
                    break;
            }
        }
    }
}
