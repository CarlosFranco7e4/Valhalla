﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicController : MonoBehaviour
{
    private Animator ani;
    public GameObject whenHeal;
    public float Heal;
    public float ManaCost;
    private bool canUseMana = true;
    // Start is called before the first frame update
    void Start()
    {
        ani = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Magic") && canUseMana && PlayerManager.Instance.Mana >= ManaCost && PlayerManager.Instance.Hp < PlayerManager.Instance.GetMaxHp())
        {
            canUseMana = false;
            StartCoroutine(Healing());
        }
    }

    IEnumerator Healing()
    {        
        ani.SetTrigger("Heal");
        whenHeal.SetActive(true);
        AudioManager.instance.Play("Heal sound");
        yield return new WaitForSeconds(0.5f);
        PlayerManager.Instance.PlayerGainHp(Heal);
        PlayerManager.Instance.PlayerUsesMagic(ManaCost);                
        whenHeal.SetActive(false);
        canUseMana = true;
    }
}
