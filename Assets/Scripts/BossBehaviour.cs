﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBehaviour : MonoBehaviour
{
    //Special behaviours
    private bool aggroOn;
    private bool healing = false;

    //move stats
    public float speed;
    public float dashForce;
    public float distanceToStop;

    //when to run
    public float whenToRunDistance;
    public float run_multiplier;

    //when to jump
    public float whenToJumpDistance;
    public float jumpForceY;
    public float jumpForceX;

    //Only before fight
    private bool idle;

    public Slider SliderBossHP;

    //Other
    private GameObject playerRef;
    private float distanceBetweenPlayerHori;
    private float distanceBetweenPlayerVertical;
    private int prob;
    private Animator ani;
    private bool iAttack = true;
    private bool death = false;
    public GameObject wall;
    public float timeToAttackSeconds = 3;
    public float timeToAttackSecondsAngry = 1;
    public float animationSpeed = 1;
    public Renderer skin;
    public Material skinAngry;
    private Rigidbody selfRB;
    private Coroutine cura;
    public float maxHp;
    public float currentHp;

    public float timeToHeal = 6;

    private Vector3 newDirection;
    void Start()
    {
        LevelManager.Instance.LevelUIController.InitBoss(this);
        playerRef = GameObject.FindGameObjectWithTag("Player");
        aggroOn = false;
        ani = gameObject.GetComponent<Animator>();
        healing = false;
        selfRB = gameObject.GetComponent<Rigidbody>();
        skin = gameObject.GetComponentInChildren<Renderer>();
        cura = StartCoroutine(waitForHeal());
        maxHp = gameObject.GetComponent<EnemyHitReceiveAndDeath>().Stats.Hp;
    }

    private void Update()
    {
        currentHp = gameObject.GetComponent<EnemyHitReceiveAndDeath>().Hp;
        if (death == false)
        {
            distanceBetweenPlayerHori = Vector3.Distance(playerRef.transform.position, transform.position);
            if (distanceBetweenPlayerHori <= 10) //Jefe pasara de idle a fight state amb una animacio
            {
                aggroOn = true;
            }
            //tot el script aqui
            if (aggroOn)
            {
                SliderBossHP.gameObject.SetActive(true);
                Debug.Log("agro");
                lookAtPlayer();
                MovementBehaviour();
                AttackBehaviour();
                MovementBehaviour();
                if (healing)
                {
                    HealSelf();
                }


                //resetea el timer de curació
                if (gameObject.GetComponent<EnemyHitReceiveAndDeath>().gotHit || gameObject.GetComponent<EnemyHitReceiveAndDeath>().dealtDamage)
                {
                    Debug.Log("Para la Cura");
                    healing = false;
                    //timeToHeal = 6;
                    StopCoroutine(cura);
                    cura = StartCoroutine(waitForHeal());
                }
            }
            if (currentHp < 50)
            {

                skin.material = skinAngry; 
                ani.SetFloat("animationSpeed", animationSpeed*1.5f);
                timeToAttackSeconds = timeToAttackSeconds = timeToAttackSecondsAngry;
            }
        }
        
    }



    public void HealSelf()
    {
        //cada 0.5 sec restaura 5% de vida
        currentHp = currentHp + 0.1f;
        if(maxHp > currentHp)
        {
            currentHp = maxHp;
        }
    }

    public void MovementBehaviour()
    {
        if (distanceBetweenPlayerHori > distanceToStop) {
            ani.SetBool("Walk", true);
            if (distanceBetweenPlayerHori > whenToRunDistance)
            {
                //run
                transform.position = Vector3.MoveTowards(transform.position, playerRef.transform.position, speed * run_multiplier * Time.deltaTime);
            }
            else
            {
                //walk
                transform.position = Vector3.MoveTowards(transform.position, playerRef.transform.position, speed * Time.deltaTime);


            }
        } else {
            ani.SetBool("Walk", false);
        }
    }

    public void AttackBehaviour()
    {
        // every 3 seconds makes a decision on how to attack depending on distance, 
        // after atack resets timer

        if (iAttack == true)
        {
        iAttack = false;
        StartCoroutine(timeToAttack(timeToAttackSeconds));
            

            if (distanceBetweenPlayerHori <= 4)
            {
                if (prob <= 6)
                {
                    //weak attack
                    ani.SetTrigger("AttackLight");
                } else
                {
                    //Strong attack
                    ani.SetTrigger("AttackStrong");
                }
            }
            else if (distanceBetweenPlayerHori > 4 && distanceBetweenPlayerHori < 7)
            {
                if (prob <= 6)
                {
                    //strong attack
                    ani.SetTrigger("AttackStrong");
                }
                else
                {
                    //jump attack
                    ani.SetTrigger("AttackJump");
                }
            }
            else
            {
                //jump attack
                ani.SetTrigger("AttackJump");
            }
        }
    }


    IEnumerator timeToAttack(float time)
    {
        yield return new WaitForSeconds(time);
        iAttack = true;
        prob = Random.Range(0, 11);
    }

    IEnumerator waitForHeal()
    {
        yield return new WaitForSeconds(4);
        healing = true;
        Debug.Log("Healing");
    }





    public void lookAtPlayer()
    {
        // Determine which direction to rotate towards
        Vector3 targetDirection = playerRef.transform.position - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = speed * 3 * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        newDirection = Vector3.RotateTowards(transform.forward, new Vector3(targetDirection.x, 0, 0), singleStep, 0.0f);

        // Draw a ray pointing at our target in
        Debug.DrawRay(transform.position, newDirection, Color.red);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    public void unlockDoubleJump()
    {
        ani.SetFloat("animationSpeed", animationSpeed);
        SliderBossHP.gameObject.SetActive(false);
        death = true;
        wall.SetActive(false);
        PlayerManager.Instance.DoubleJumpsUnlocked = true;
    }

    public void jumpAttack()
    {
        if(playerRef.transform.position.x < transform.position.x)
        {
            GetComponent<Rigidbody>().velocity = new Vector2(-jumpForceX, jumpForceY);
        }
        else
        {
        GetComponent<Rigidbody>().velocity = new Vector2(jumpForceX, jumpForceY);
        }
    }

    public float GetMaxHp()
    {
        return maxHp;
    }
    public float GetHp()
    {
        return gameObject.GetComponent<EnemyHitReceiveAndDeath>().Hp;
    }
}
